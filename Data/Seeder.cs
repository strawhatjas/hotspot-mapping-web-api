﻿using hotspot_mapping_web_api.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace hotspot_mapping_web_api.Data
{
    public static class Seeder
    {
        public static void InitializeRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Admin";
                IdentityResult roleResult = roleManager.
                    CreateAsync(role).Result;
            }
        }

        public static void InitializeUsers(UserManager<User> userManager)
        {
            if (userManager.FindByEmailAsync("admin").Result == null)
            {
                User user = new User();
                user.UserName = "admin";
                user.Email = "admin";
                user.FirstName = "Admin";
                user.LastName = "";

                IdentityResult result = userManager.CreateAsync(user, "P@ssw0rd1!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }
        }

        public static void InitializeHotspots(IServiceProvider serviceProvider)
        {
            using (var context = new HotspotDbContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<HotspotDbContext>>()))
            {
                Console.WriteLine("Seeder started...");
                // Look for any hotspots.
                if (context.Hotspots.Any())
                {
                    Console.WriteLine("DB has already been seeded...");
                    return;   // DB has been seeded
                }

                Console.WriteLine("Adding new hotspots...");
                context.Hotspots.AddRange(
                    new Hotspot
                    {
                        SiteName = "Alberton Café",
                        Address = "198 Bridport St",
                        SuburbName = "Albert Park",
                        Postcode = "3206",
                        State = "VIC",
                        Country = "Australia",
                        Latitude = -37.841030,
                        Longitude = 144.953770,
                        GeoCoordinate = new Point(144.953770, -37.841030) { SRID = 4326 },
                        StartDate = DateTime.ParseExact("2021-05-01 08:50", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        EndDate = DateTime.ParseExact("2021-05-01 10:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        Note = "Case dined at venue"
                    },

                    new Hotspot
                    {
                        SiteName = "North Point Cafe",
                        Address = "2B North Rd",
                        SuburbName = "Brighton",
                        Postcode = "3186",
                        State = "VIC",
                        Country = "Australia",
                        Latitude = -37.8977581,
                        Longitude = 144.9864129,
                        GeoCoordinate = new Point(144.986590, -37.897700) { SRID = 4326 },
                        StartDate = DateTime.ParseExact("2021-05-01 08:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        EndDate = DateTime.ParseExact("2021-05-01 09:30", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        Note = "Case dined outside and used bathroom"
                    }
                ); ;
                context.SaveChanges();
                Console.WriteLine("New hotspots added...");
            }
        }
    }
}
