﻿using hotspot_mapping_web_api.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hotspot_mapping_web_api.Data
{
    public class HotspotDbContext : IdentityDbContext<User>
    {
        public HotspotDbContext(DbContextOptions<HotspotDbContext> options)
            : base(options)
        {
        }

        public DbSet<Hotspot> Hotspots { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
