﻿using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace hotspot_mapping_web_api.Entities
{
    public class Hotspot
    {
        // id
        public int Id { get; set; }

        // site name
        [Required]
        [MaxLength(50, ErrorMessage = "Site Name must be 50 characters or less")]
        public string SiteName { get; set; }

        // address
        [Required]
        [MaxLength(100, ErrorMessage = "Address must be 100 characters or less")]
        public string Address { get; set; }

        // suburb name
        [Required]
        [MaxLength(50, ErrorMessage = "Suburb Name must be 50 characters or less")]
        public string SuburbName { get; set; }

        // postcode
        [Required]
        [RegularExpression(@"^(?!0000)[0-9]{4,4}$", ErrorMessage = "Post code must contain 4 digits and should not be 0000")]
        public string Postcode { get; set; }

        // state : default to VIC
        [Required]
        [MaxLength(50, ErrorMessage = "State must be 50 characters or less")]
        public string State { get; set; }

        // country : default to AUS
        [Required]
        [MaxLength(50, ErrorMessage = "Country must be 50 characters or less")]
        public string Country { get; set; }

        // latitude
        [Required]
        public double Latitude { get; set; }

        // longitude
        [Required]
        public double Longitude { get; set; }

        [Required]
        [JsonIgnore]
        public Point GeoCoordinate { get; set; }

        // start date
        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        // end date
        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        // note
        [MaxLength(500, ErrorMessage = "Note must be 500 characters or less")]
        public string Note { get; set; }
    }
}
