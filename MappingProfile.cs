﻿using AutoMapper;
using hotspot_mapping_web_api.DTOs;
using hotspot_mapping_web_api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hotspot_mapping_web_api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<HotspotDTO, Hotspot>();
        }
    }
}
