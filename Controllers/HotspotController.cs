﻿using AutoMapper;
using hotspot_mapping_web_api.Data;
using hotspot_mapping_web_api.DTOs;
using hotspot_mapping_web_api.Entities;
using hotspot_mapping_web_api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;

namespace hotspot_mapping_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HotspotController : ControllerBase
    {
        private readonly HotspotDbContext _context;
        private readonly HotspotService _hotspotService;
        private IMapper _mapper;

        public HotspotController(
            HotspotDbContext context,
            HotspotService hotspotService,
            IMapper mapper)
        {
            _context = context;
            _hotspotService = hotspotService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a specific hotspot record
        /// </summary>
        /// <param name="id">ID of the hotspot record</param>
        /// <returns>Hotspot object</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Hotspot>> Get(int id)
        {
            var item = await _context.Hotspots.FindAsync(id);

            if (item == null)
            {
                return BadRequest(new { message = "Hotspot record not found" });
            }

            return Ok(item);
        }

        /// <summary>
        /// Gets all the hotspot records
        /// </summary>
        /// <returns>Hotspot list</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Hotspot>>> GetAll()
        {
            return Ok(await _context.Hotspots.ToListAsync());
        }

        /// <summary>
        /// Gets all the hotspot records within distance and date range
        /// </summary>
        /// <param name="range">Distance to cover, in meters</param>
        /// <param name="latitude">Latitude of user's current location</param>
        /// <param name="longitude">Longitude of user's current location</param>
        /// <param name="fromDate">Start date of date range. Format: yyyy-MM-dd. Defaults to 12:00AM.</param>
        /// <param name="toDate">End date of date range. Format: yyyy-MM-dd. Defaults to 11:59PM.</param>
        /// <returns></returns>
        [HttpGet("{range}/{latitude}/{longitude}/{fromDate}/{toDate}")]
        public async Task<ActionResult<IEnumerable<Hotspot>>> GetAllWithinRange(double range, double latitude, double longitude, DateTime fromDate, DateTime toDate)
        {
            var userLocation = new Point(longitude, latitude) { SRID = 4326 };

            // TODO: only for the past 14 days
            return Ok(await _context.Hotspots
                .Where(h => h.GeoCoordinate.Distance(userLocation) <= range
                            && fromDate.CompareTo(h.StartDate) <= 0
                            && toDate.AddHours(23).AddMinutes(59).AddSeconds(59).CompareTo(h.StartDate) >= 0)
                .ToListAsync());
        }

        /// <summary>
        /// Gets all the hotspot records within the date range
        /// </summary>
        /// <param name="fromDate">Start date of date range. Format: yyyy-MM-dd. Defaults to 12:00AM.</param>
        /// <param name="toDate">End date of date range. Format: yyyy-MM-dd. Defaults to 11:59PM.</param>
        /// <returns>Hotspot list within the date range</returns>
        [HttpGet("{fromDate}/{toDate}")]
        public async Task<ActionResult<IEnumerable<Hotspot>>> GetByDateRange(DateTime fromDate, DateTime toDate)
        {
            return Ok(await _context.Hotspots
                .Where(h => fromDate.CompareTo(h.StartDate) <= 0 && toDate.AddHours(23).AddMinutes(59).AddSeconds(59).CompareTo(h.StartDate) >= 0)
                .ToListAsync());
        }

        /// <summary>
        /// Adds a hotspot record
        /// </summary>
        /// <param name="hotspotDTO">Please refer to the schemas</param>
        /// <returns>Returns the newly added hotspot record</returns>
        [HttpPost]
        public async Task<ActionResult<Hotspot>> Post(HotspotDTO hotspotDTO)
        {
            if (hotspotDTO == null)
            {
                return BadRequest(new { message = "HotspotDTO object is null" });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = "Invalid model object" });
            }

            Hotspot hotspot = _mapper.Map<Hotspot>(hotspotDTO);
            hotspot.Country = "Australia";
            hotspot.State = "VIC";

            try
            {
                var newHotspot = await _hotspotService.GenerateLatLng(hotspot);
                newHotspot.GeoCoordinate = new Point(newHotspot.Longitude, newHotspot.Latitude) { SRID = 4326 };

                // check for duplicates
                // duplicate would be the same: latitude, longitude, start date and end date
                var duplicate = _context.Hotspots.Where(h => h.Latitude == newHotspot.Latitude
                                    && h.Longitude == newHotspot.Longitude
                                    && h.StartDate.CompareTo(newHotspot.StartDate) == 0
                                    && h.EndDate.CompareTo(newHotspot.EndDate) == 0)
                                .FirstOrDefault();

                if (duplicate != null)
                {
                    throw new Exception("Hotspot record is a duplicate");
                }

                await _context.Hotspots.AddAsync(newHotspot);
                await _context.SaveChangesAsync();

                return Ok(newHotspot);
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }

        /// <summary>
        /// Updates a hotspot record
        /// </summary>
        /// <param name="id">ID of the hotspot record</param>
        /// <param name="hotspotDTO">Please refer to the schemas</param>
        /// <returns>Returns the updated hotspot record</returns>
        [HttpPut("{id}")]
        [ActionName("Put")]
        public async Task<ActionResult<Hotspot>> Put(int id, HotspotDTO hotspotDTO)
        {
            if (id != hotspotDTO.Id)
            {
                return BadRequest(new { message = "Hotspot ID does not match submitted data" });
            }

            var item = await _context.Hotspots.FindAsync(id);

            if (item != null)
            {
                foreach (PropertyInfo propSource in hotspotDTO.GetType().GetProperties())
                {
                    item.GetType().GetProperty(propSource.Name).SetValue(item, propSource.GetValue(hotspotDTO, null));
                }

                // TODO: Update longitude and latitude
                item = await _hotspotService.GenerateLatLng(item);
                item.GeoCoordinate = new Point(item.Longitude, item.Latitude) { SRID = 4326 };

                _context.Entry(item).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();

                    return Ok(item);
                }
                catch (DbUpdateConcurrencyException dbEx)
                {
                    return BadRequest(new { message = dbEx.Message });
                }
                catch (Exception ex)
                {
                    return BadRequest(new { message = ex.Message });
                }
            }
            else
            {
                return NotFound(new { message = "Hotspot record not found" });
            }
        }

        /// <summary>
        /// Deletes a hotspot record
        /// </summary>
        /// <param name="id">ID of the hotpost record to be deleted</param>
        /// <returns>Returns OkResult if hotspot record is deleted</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await _context.Hotspots.FindAsync(id);
            if (item == null)
            {
                return NotFound(new { message = "Hotspot record not found" });
            }

            _context.Hotspots.Remove(item);

            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Delete all hotspot records
        /// </summary>
        /// <returns>Returns OkResult if all hotspot records are deleted</returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteAll()
        {
            var items = await _context.Hotspots.ToListAsync();
            if (items.Any())
            {
                foreach(var item in items)
                {
                    _context.Hotspots.Remove(item);
                }

                await _context.SaveChangesAsync();

                return Ok();
            }
            else
            {
                return NotFound(new { message = "There is no hotspot record" });
            }
        }

        /// <summary>
        /// Invokes DHHS Website Parser Application and updates the hotspots list
        /// </summary>
        /// <returns></returns>
        [HttpGet("Refresh")]
        public async Task<ActionResult> Refresh()
        {
            try
            {
                // Call nodejs parser function here
                string url = "https://hotspotmapping-parser-dev.azurewebsites.net/adminrefresh";
                WebRequest request = HttpWebRequest.Create(url);
                WebResponse response = await request.GetResponseAsync();

                await Task.Delay(5000);

                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(new { message = "Cannot refresh hotspot list. Reason: " + ex.Message });
            }
        }
    }
}
