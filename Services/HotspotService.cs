﻿using hotspot_mapping_web_api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace hotspot_mapping_web_api.Services
{
    public class HotspotService
    {
        public async Task<Hotspot> GenerateLatLng(Hotspot hotspot)
        {
            hotspot.SiteName = HttpUtility.HtmlDecode(hotspot.SiteName);
            hotspot.Address = HttpUtility.HtmlDecode(hotspot.Address);
            hotspot.State = HttpUtility.HtmlDecode(hotspot.State);

            var address = hotspot.SiteName + " " + hotspot.Address + " " + hotspot.SuburbName + " " + hotspot.State + " " + hotspot.Postcode;

            string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&sensor=false&key=AIzaSyCSgVJtVf4oKXEowR3G3RvE5XRn-EC3fNE";

            try
            {
                using (var client = new HttpClient())
                {
                    var result = await client.GetAsync(url);

                    if (result.IsSuccessStatusCode)
                    {
                        XmlDocument doc = new XmlDocument();
                        var xml = await result.Content.ReadAsStringAsync();
                        doc.LoadXml(xml);

                        var addressComponents = doc.GetElementsByTagName("address_component");
                        foreach (XmlNode component in addressComponents)
                        {
                            if (component.SelectSingleNode("type").InnerText == "postal_code")
                            {
                                if(hotspot.Postcode == null || hotspot.Postcode == "")
                                    hotspot.Postcode = component.SelectSingleNode("long_name").InnerText;
                            }
                        }

                        var geometry = doc.GetElementsByTagName("geometry");
                        hotspot.Latitude = Double.Parse(geometry.Item(0).SelectSingleNode("location/lat").InnerText);
                        hotspot.Longitude = Double.Parse(geometry.Item(0).SelectSingleNode("location/lng").InnerText);

                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Cannot generate latitude and longitude");
            }

            return hotspot;
        }
    }
}
