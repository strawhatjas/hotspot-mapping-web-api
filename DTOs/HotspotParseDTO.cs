﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hotspot_mapping_web_api.DTOs
{
    public class HotspotParseDTO
    {
        // site name
        [Required]
        [MaxLength(50, ErrorMessage = "Site Name must be 50 characters or less")]
        public string Location { get; set; }

        // address
        [Required]
        [MaxLength(200, ErrorMessage = "Address must be 100 characters or less")]
        public string Site { get; set; }

        // start date
        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        // end date
        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
    }
}
