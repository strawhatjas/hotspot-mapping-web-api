﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hotspot_mapping_web_api.DTOs
{
    public class HotspotDTO
    {
        public int Id { get; set; }

        // site name
        [Required]
        [MaxLength(50, ErrorMessage = "Site Name must be 50 characters or less")]
        public string SiteName { get; set; }

        // address
        [Required]
        [MaxLength(100, ErrorMessage = "Address must be 100 characters or less")]
        public string Address { get; set; }

        // suburb name
        [Required]
        [MaxLength(50, ErrorMessage = "Suburb Name must be 50 characters or less")]
        public string SuburbName { get; set; }

        // postcode
        [RegularExpression(@"^(?!0000)[0-9]{4,4}$", ErrorMessage = "Post code must contain 4 digits and should not be 0000")]
        public string Postcode { get; set; }

        // start date
        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        // end date
        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        // note
        [MaxLength(500, ErrorMessage = "Note must be 500 characters or less")]
        public string Note { get; set; }
    }
}
